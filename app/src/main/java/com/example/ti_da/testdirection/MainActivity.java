package com.example.ti_da.testdirection;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.nfc.Tag;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

import static android.hardware.Sensor.TYPE_GYROSCOPE;

public class MainActivity extends AppCompatActivity {
    String TAG = "test";
    TextView timer, tv3;
    String data = "";
    Button btnStart, btnStop;
    Button btnRefresh;
    String dataGyro="";
    String dataAcc="";
    DecimalFormat df = new DecimalFormat("#.00");
    int count = 0;
    Timer T;
    EditText edtFileName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        timer = (TextView) findViewById(R.id.timer);
        tv3 = (TextView) findViewById(R.id.test3);
        btnStart = (Button) findViewById(R.id.start);
        btnStop = (Button) findViewById(R.id.stop);
        btnRefresh = (Button) findViewById(R.id.refresh);
        edtFileName = (EditText) findViewById(R.id.edtFileName);

        final SensorManager sensorManager =
                (SensorManager) getSystemService(SENSOR_SERVICE);
        final Sensor Gy =
                sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        final Sensor Acce = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        final SensorEventListener lv = new SensorEventListener(){
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                switch (sensorEvent.sensor.getType()){
                    case Sensor.TYPE_GYROSCOPE:
                        dataAcc += String.valueOf(df.format(sensorEvent.values[0])) + "   " + String.valueOf(df.format(sensorEvent.values[1])) + "   " + String.valueOf(df.format(sensorEvent.values[2])) + "   " + String.valueOf(df.format(dataTotal(sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]))) + "   " + "\n";
                        break;
                    case Sensor.TYPE_ACCELEROMETER:
                        dataGyro += String.valueOf(df.format(sensorEvent.values[0])) + "   " + String.valueOf(df.format(sensorEvent.values[1])) + "   " + String.valueOf(df.format(sensorEvent.values[2])) + "   " + String.valueOf(df.format(dataTotal(sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2]))) + "   " + "\n";
                        data += "x: " + String.valueOf(df.format(sensorEvent.values[0])) + "   " + "y:" + String.valueOf(df.format(sensorEvent.values[1])) + "    " + "z:" + String.valueOf(df.format(sensorEvent.values[2])) + "    "  + "total: " +  String.valueOf(df.format(Math.sqrt(sensorEvent.values[0]*sensorEvent.values[0] + sensorEvent.values[1]*sensorEvent.values[1] + sensorEvent.values[2]*sensorEvent.values[2]))) + " " + "\n";
                        tv3.setText(String.valueOf(data));
                        break;

                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {

            }
        };


        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sensorManager.unregisterListener(lv);
                T.cancel();
                writeTofile(edtFileName.getText().toString().trim() + "Acc", dataAcc);
                writeTofile(edtFileName.getText().toString().trim() + "Gyro", dataGyro);

            }
        });
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edtFileName.getText().toString().trim().equals("")){
                    Toast.makeText(getApplication(), "Bạn cần phải nhập tên của file dữ liệu", Toast.LENGTH_SHORT).show();
                }else{
                    T =new Timer();
                    T.scheduleAtFixedRate(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    timer.setText(String.valueOf(count));
                                    count++;
                                }
                            });
                        }
                    }, 1000, 1000);
                    sensorManager.registerListener(lv, Acce, 1000000);
                    sensorManager.registerListener(lv, Gy, 1000000);
                }
            }
        });

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count = 0;
                data = "";
                tv3.setText(String.valueOf(data));
                timer.setText(String.valueOf(count));
                edtFileName.setText("");
            }
        });
    }
    static float dataTotal(float x, float y, float z){
        return (float) Math.sqrt(x*x + y*y + z*z);
    }

    public void writeTofile(String fileName, String data){
        // Get the directory for the user's public pictures directory.
        final File path =
                Environment.getExternalStoragePublicDirectory
                        (
                                //Environment.DIRECTORY_PICTURES
                                Environment.DIRECTORY_DCIM + "/SensorData/"
                        );

        // Make sure the path directory exists.
        if(!path.exists())
        {
            // Make it, if it doesn't exit
            path.mkdirs();
        }
        final File file = new File(path, fileName + ".txt");
        // Save your stream, don't forget to flush() it before closing it.
        try
        {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(data);
            myOutWriter.close();
            fOut.flush();
            fOut.close();
        }
        catch (IOException e)
        {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}
